// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
// <-- JVM NOT MANDATORY -->

// <-- Non-regression test for bug 353 -->
//
// <-- URL -->
//  http://forge.scilab.org/index.php/p/csv-readwrite/issues/353/
//
// <-- Short Description -->
// The csv_read function does not manage the range.

path = fullfile(csv_getToolboxPath(),"tests","unit_tests");

//
// Read only rows/columns in range
r = csv_read(fullfile(path,"K_1.csv"), [], [], "double" , [] , [], [2 1 3 2] );
expected = [
0.10000000000000001 1.1000000000000001
0.10000000000000001 0.10000000000000001
];
assert_checkequal ( r , expected );
//
//  Not symetric range
r = csv_read(fullfile(path,"K_1.csv"), [], [], "double" , [] , [], [2 1 2 2] );
expected = [
0.10000000000000001 1.1000000000000001
];
assert_checkequal ( r , expected );
//
// Read complex doubles
r = csv_read(fullfile(path,"K_1.csv"), [], [], "string" , [] , [], [2 1 3 2] );
expected = [
"0.10000000000000001" "1.1000000000000001"
"0.10000000000000001" "0.10000000000000001"
];
assert_checkequal ( r , expected );
//
// Not symetric range
r = csv_read(fullfile(path,"K_1.csv"), [], [], "string" , [] , [], [2 1 2 2] );
expected = [
"0.10000000000000001" "1.1000000000000001"
];
assert_checkequal ( r , expected );
//
// Unconsistent range: C2 < C1
instr = "r = csv_read(fullfile(path,""K_1.csv""), [], [], ""string"" , [] , [], [2 3 3 2] );";
assert_checkerror ( instr , "csv_read: Wrong value for input argument #7: Unconsistent range." );
//
// Unconsistent range: R2 < R1
instr = "r = csv_read(fullfile(path,""K_1.csv""), [], [], ""string"" , [] , [], [3 1 2 2] );";
assert_checkerror ( instr , "csv_read: Wrong value for input argument #7: Unconsistent range." );
//
// Non-integer indice
instr="r = csv_read(fullfile(path,""K_1.csv""), [], [], ""string"" , [] , [], [2 1 1.5 2] );";
assert_checkerror ( instr , "csv_read: Wrong value for input argument #7: A matrix of double, with integer values, expected." );
//
// Infinite indice
instr="r = csv_read(fullfile(path,""K_1.csv""), [], [], ""string"" , [] , [], [2 1 %inf 2] );";
assert_checkerror ( instr , "csv_read: Wrong value for input argument #7: A matrix of double, with integer values, expected." );
//
// Row indice larger than actual number of rows: string case
r = csv_read(fullfile(path,"K_1.csv"), [], [], "string" , [] , [], [2 1 999 2] );
expected = [
"0.10000000000000001" "1.1000000000000001"
"0.10000000000000001" "0.10000000000000001"
];
assert_checkequal ( r , expected );
//
// Column indice larger than actual number of columns: string case
r = csv_read(fullfile(path,"K_1.csv"), [], [], "string" , [] , [], [2 1 3 999] );
expected = [
"0.10000000000000001" "1.1000000000000001"
"0.10000000000000001" "0.10000000000000001"
];
assert_checkequal ( r , expected );
//
// Row indice larger than actual number of rows: double case
r = csv_read(fullfile(path,"K_1.csv"), [], [], "double" , [] , [], [3 1 999 2] );
expected = [
0.10000000000000001 0.10000000000000001
];
assert_checkequal ( r , expected );
//
// Column indice larger than actual number of columns: double case
r = csv_read(fullfile(path,"K_1.csv"), [], [], "double" , [] , [], [2 2 3 999] );
expected = [
1.1000000000000001
0.10000000000000001
];
assert_checkequal ( r , expected );

// r = csv_read(fullfile(path,"complexdata.csv"), [], [], "double" , [] , [], [2 1 3 2] );
// Fails due to ticket #360
// TODO : range of doubles complex 
// TODO : Row indice larger than actual number of rows: complex double case
// TODO : Col indice larger than actual number of columns: complex double case


