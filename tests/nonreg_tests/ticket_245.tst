// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
// <-- JVM NOT MANDATORY -->

// <-- Non-regression test for bug 245 -->
//
// <-- URL -->
//  http://forge.scilab.org/index.php/p/csv-readwrite/issues/245/
//
// <-- Short Description -->
// csv_stringtodouble failed on some special cases.

path = fullfile(csv_getToolboxPath(),"tests","unit_tests");

a = csv_stringtodouble("3.14159e-10+inf");
assert_checkequal ( isnan(a) , %t );

assert_checkequal ( execstr("a = csv_stringtodouble(""3.14159e-10+inf"", %f);", "errcatch") , 999 );
assert_checkequal ( lasterror() , msprintf(_("%s: can not convert data.\n"), "csv_stringtodouble") );

a = csv_stringtodouble("6+3*I");
assert_checkequal ( isnan(a) , %t );

assert_checkequal ( execstr("a = csv_stringtodouble(""6+3*I"", %f);", "errcatch") , 999 );
assert_checkequal ( lasterror() , msprintf(_("%s: can not convert data.\n"), "csv_stringtodouble") );
